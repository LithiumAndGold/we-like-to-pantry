from django.urls import path
from . import views
from accounts.views import signup, userlogin, userlogout


app_name = 'accounts'

urlpatterns = [
    path("signup/", signup, name="signup"),
    path("login/", userlogin, name="login"),
    path("logout/", userlogout, name="logout"),
    path('csrf-failure/', views.csrf_failure_view, name='csrf_failure'),
]
