from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from accounts.forms import SignUpForm, LoginForm
from django.views.decorators.csrf import requires_csrf_token, csrf_protect


@csrf_protect
def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            password_confirmation = form.cleaned_data['password_confirmation']

            if password == password_confirmation:
                user = User.objects.create_user(
                    username,
                    password=password
                )

                login(request, user)

                return redirect("home")
            else:
                form.add_error("password", "Passwords do not match")

    else:
        form = SignUpForm()
        context = {"form": form}
        return render(request, "accounts/signup.html", context)


def userlogin(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(
                request,
                username=username,
                password=password,
            )
            if user is not None:
                login(request, user)
                return redirect("home")

    else:
        form = LoginForm()
        context = {"form": form}
        return render(request, "accounts/login.html", context)


def userlogout(request):
    logout(request)
    return redirect("home")


@requires_csrf_token
def csrf_failure_view(request, reason=""):
    return render(request, "csrf_failure.html", {"reason": reason})
