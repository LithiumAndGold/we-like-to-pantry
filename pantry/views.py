from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from pantry.forms import CreatePantryForm, CreateIngredientForm
from pantry.models import Pantry, Ingredient


def home(request):
    return render(request, "pantry/home.html")


@login_required
def list_of_pantries(request):
    pantry_list = Pantry.objects.all()
    context = {"pantry_list": pantry_list}
    return render(request, "pantry/pantry.html", context)


@login_required
def list_of_ingredients(request):
    ingredient_list = Ingredient.objects.all()
    context = {"ingredient_list": ingredient_list}
    return render(request, "pantry/pantryingredients.html", context)


@login_required
def create_pantry(request):
    if request.method == "POST":
        form = CreatePantryForm(request.POST)
        if form.is_valid():
            create_pantry = form.save(False)
            create_pantry.owner = request.user
            create_pantry.save()
            return redirect("create_pantry")
    else:
        form = CreatePantryForm()

    context = {"form": form}
    return render(request, "pantry/createpantry.html", context)


@login_required
def edit_pantry(request, id):
    pantry = get_object_or_404(Pantry, id=id)
    if request.method == "POST":
        form = CreatePantryForm(request.POST, instance=pantry)
        if form.is_valid():
            form.save()
            return redirect("pantrylist")
    else:
        form = CreatePantryForm(instance=pantry)
    context = {
        "post_object": pantry,
        "post_form": form,
    }
    return render(request, "pantry/editpantry.html", context)


@login_required
def create_ingredient(request):
    if request.method == "POST":
        form = CreateIngredientForm(request.POST)
        if form.is_valid():
            create_ingredient = form.save(False)
            create_ingredient.owner = request.user
            create_ingredient.save()
            return redirect("create_ingredient")
    else:
        form = CreateIngredientForm()

    context = {"form": form}
    return render(request, "pantry/createingredient.html", context)


@login_required
def edit_ingredient(request, id):
    ingredient = get_object_or_404(Ingredient, id=id)
    if request.method == "POST":
        form = CreateIngredientForm(request.POST, instance=ingredient)
        if form.is_valid():
            form.save()
            return redirect("pantryingredients")
    else:
        form = CreateIngredientForm(instance=ingredient)
    context = {
        "post_object": ingredient,
        "post_form": form,
    }
    return render(request, "pantry/editingredient.html", context)
