from django.db import models
from django.contrib.auth.models import User


class Pantry(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey(
        User,
        related_name="pantry",
        on_delete=models.CASCADE,
        null=True
    )

    def __str__(self):
        return self.name


class Measurement(models.Model):
    name = models.CharField(max_length=50)
    owner = models.ForeignKey(
        User,
        related_name="measurement",
        on_delete=models.CASCADE,
        null=True
    )

    def __str__(self):
        return self.name


class Ingredient(models.Model):
    name = models.CharField(max_length=100)
    quantity = models.IntegerField()
    measurement = models.ForeignKey(
        Measurement,
        related_name="measurement",
        on_delete=models.CASCADE,
    )
    expiration_date = models.DateField()
    created_on = models.DateTimeField(auto_now_add=True)
    type_of_pantry = models.ManyToManyField(Pantry)
    owner = models.ForeignKey(
        User,
        related_name="ingredient",
        on_delete=models.CASCADE,
        null=True
    )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]
