from django import forms
from pantry.models import Pantry, Ingredient, Measurement


class CreatePantryForm(forms.ModelForm):
    class Meta:
        model = Pantry
        fields = [
            "name",
            "description",
        ]


class CreateIngredientForm(forms.ModelForm):
    new_measurement = forms.CharField(max_length=50, required=False)

    class Meta:
        model = Ingredient
        fields = [
            "name",
            "quantity",
            "measurement",
            "new_measurement",
            "expiration_date",
            "type_of_pantry",
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['measurement'].required = False

    def clean(self):
        cleaned_data = super().clean()
        new_measurement = cleaned_data.get('new_measurement')
        measurement = cleaned_data.get('measurement')
        if not measurement and not new_measurement:
            raise forms.ValidationError("You must either select a measurement or enter a new one.")
        return cleaned_data

    def save(self, commit=True):
        ingredient = super().save(commit=False)
        new_measurement = self.cleaned_data.get('new_measurement')
        if new_measurement:
            # If a new measurement was entered, create a new Measurement object
            # or get the existing one if it already exists
            measurement, _ = Measurement.objects.get_or_create(name=new_measurement)
            ingredient.measurement = measurement
        if commit:
            ingredient.save()
        return ingredient
