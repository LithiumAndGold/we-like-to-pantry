from django.contrib import admin
from pantry.models import Pantry, Ingredient, Measurement


class PantryAdmin(admin.ModelAdmin):
    list_display = (
       "name",
       "description",
       "created_on",
    )


admin.site.register(Pantry, PantryAdmin)


class IngredientAdmin(admin.ModelAdmin):
    list_display = (
       "name",
       "quantity",
       "measurement",
       "expiration_date",
       "created_on",
    )


admin.site.register(Ingredient, IngredientAdmin)


class MeasurementAdmin(admin.ModelAdmin):
    list_display = (
       "name",
    )


admin.site.register(Measurement, MeasurementAdmin)
