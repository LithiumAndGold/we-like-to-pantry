from django.urls import include, path
from pantry.views import home, list_of_pantries, list_of_ingredients
from pantry.views import create_pantry, create_ingredient, edit_pantry
from pantry.views import edit_ingredient


urlpatterns = [
    path('', home, name="home"),
    path("pantry/", list_of_pantries, name="pantrylist"),
    path("pantryingredients/", list_of_ingredients, name="pantryingredients"),
    path("createpantry/", create_pantry, name="create_pantry"),
    path("createingredient/", create_ingredient, name="create_ingredient"),
    path("<int:id>/editpantry/", edit_pantry, name="edit_pantry"),
    path("<int:id>/editingredient/", edit_ingredient, name="edit_ingredient"),
    path('', include(('accounts.urls', 'accounts'), namespace="accounts")),
]
